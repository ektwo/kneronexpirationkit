#ifndef INCLUDE_GIOVANNI_DICANIO_WINREG_HPP
#define INCLUDE_GIOVANNI_DICANIO_WINREG_HPP

#include <Windows.h>        
#include <stdexcept>        
#include <string>           
#include <utility>          


class WinRegAPI
{
public:

    WinRegAPI(HKEY hKeyParent, const std::wstring& subKey);
    ~WinRegAPI() noexcept;

    bool IsValid() const noexcept;
    void Close() noexcept;
    bool Create(HKEY hKeyParent, const std::wstring& subKey, REGSAM desiredAccess = KEY_READ | KEY_WRITE);

    bool Create(HKEY hKeyParent,const std::wstring& subKey, REGSAM desiredAccess, DWORD options, SECURITY_ATTRIBUTES *securityAttributes, DWORD *disposition);

    bool Open(HKEY hKeyParent, const std::wstring& subKey, REGSAM desiredAccess = KEY_READ | KEY_WRITE);

    bool SetBinaryValue(const std::wstring& valueName, const BYTE *data, size_t dataSize);
    bool GetBinaryValue(const std::wstring& valueName, BYTE **lppData, size_t *lpDataSize);

    bool DeleteValue(const std::wstring& valueName);
    bool DeleteKey(const std::wstring& subKey, REGSAM desiredAccess);

private:
    HKEY m_hKey{ nullptr };
};

inline WinRegAPI::WinRegAPI(const HKEY hKeyParent, const std::wstring& subKey)
{
    //bool bCreate = Create(hKeyParent, subKey);
    //qDebug("bCreate=%d", bCreate);
}

inline WinRegAPI::~WinRegAPI() noexcept{
    Close();
}

inline void WinRegAPI::Close() noexcept{
    if (IsValid())
    {
        ::RegCloseKey(m_hKey);
        m_hKey = nullptr;
    }
}

inline bool WinRegAPI::IsValid() const noexcept{
    return m_hKey != nullptr;
}

inline bool WinRegAPI::Create(const HKEY hKeyParent, const std::wstring& subKey, const REGSAM desiredAccess){
    constexpr DWORD kDefaultOptions = REG_OPTION_NON_VOLATILE;

    return Create(hKeyParent, subKey, desiredAccess, kDefaultOptions, nullptr, nullptr);
}

inline bool WinRegAPI::Create(const HKEY hKeyParent,
                              const std::wstring& subKey,
                              const REGSAM desiredAccess,
                              const DWORD options,
                              SECURITY_ATTRIBUTES* const securityAttributes,
                              DWORD* const disposition){
    bool ret = false;
    HKEY hKey{ nullptr };
    LONG retCode =
    ::RegCreateKeyEx(hKeyParent,subKey.c_str(),0,REG_NONE,options,desiredAccess,securityAttributes,&hKey,disposition);
    if (ERROR_SUCCESS == retCode){
        Close();
        m_hKey = hKey;
        ret = true;
    }

    return ret;
}

inline bool WinRegAPI::Open(const HKEY hKeyParent, const std::wstring& subKey, const REGSAM desiredAccess) {
    bool ret = false;
    HKEY hKey{ nullptr };

    LONG retCode = ::RegOpenKeyEx(hKeyParent, subKey.c_str(), REG_NONE, desiredAccess, &hKey);
    if (ERROR_SUCCESS == retCode) {
        Close();
        m_hKey = hKey;
        ret = true;
    }

    return ret;
}

inline bool WinRegAPI::SetBinaryValue(const std::wstring& valueName, const BYTE* data, size_t dataSize){
    if (!IsValid()) return false;
    bool ret = false;
    LONG retCode = ::RegSetValueEx(m_hKey, valueName.c_str(), 0, REG_BINARY, static_cast<const BYTE*>(data), (DWORD)dataSize);
    if (ERROR_SUCCESS == retCode)
        ret = true;

    return ret;
}

inline bool WinRegAPI::GetBinaryValue(const std::wstring& valueName, BYTE **lppData, size_t *lpDataSize){
    if (!IsValid()) return false;
    if (!lppData) return false;
    if (!lpDataSize) return false;

    bool ret = false;

    DWORD dataSize = 0; 
    const DWORD flags = RRF_RT_REG_BINARY;
    LONG retCode = ::RegGetValue(m_hKey, nullptr, valueName.c_str(), flags, nullptr, nullptr, &dataSize);
    if (ERROR_SUCCESS == retCode) {
        *lppData = (BYTE*)malloc(sizeof(BYTE) * dataSize);
        if (*lppData) {
            retCode = ::RegGetValue(m_hKey, nullptr, valueName.c_str(), flags, nullptr, *lppData, &dataSize);
            if (ERROR_SUCCESS == retCode) {
                *lpDataSize = dataSize;
                ret = true;
            }
            else
                free(*lppData);
        }
    }

    return ret;
}

inline bool WinRegAPI::DeleteValue(const std::wstring& valueName){
    if (!IsValid()) return false;

    bool ret = false;
    LONG retCode = ::RegDeleteValue(m_hKey, valueName.c_str());
    if (ERROR_SUCCESS == retCode)
        ret = true;

    return ret;
}

inline bool WinRegAPI::DeleteKey(const std::wstring& subKey, const REGSAM desiredAccess){
    if (!IsValid()) return false;

    bool ret = false;

    LONG retCode = ::RegDeleteKeyEx(m_hKey, subKey.c_str(), KEY_ALL_ACCESS, 0);// | KEY_WOW64_64KEY, 0);
    if (ERROR_SUCCESS == retCode)
        ret = true;

    return ret;
}

#endif // INCLUDE_GIOVANNI_DICANIO_WINREG_HPP
