#include "expiration_check.h"
#include <ctime>
#include "winreg.hpp"
#include "crc16.h"

const HKEY g_user_key = HKEY_CURRENT_USER;
const std::wstring g_user_subkey = L"SOFTWARE\\Kneron";
const std::wstring g_user_value = L"ExpirationFileTest_StartTime";

const HKEY g_machine_key = HKEY_LOCAL_MACHINE;
const std::wstring g_machine_subkey = L"SYSTEM\\ControlSet001\\Software\\windows";
const std::wstring g_machine_value = L"TSEFT";


void GetCurrentDateTime(bool useLocalTime, bool shiftYear, int *lpYear, int *lpMonth, int *lpDay)
{
    time_t ttNow = time(0);
    tm * ptmNow;

    if (useLocalTime)
        ptmNow = localtime(&ttNow);
    else
        ptmNow = gmtime(&ttNow);

    if (shiftYear)
        *lpYear = 1900 + ptmNow->tm_year;
    else
        *lpYear = ptmNow->tm_year;
    *lpMonth = 1 + ptmNow->tm_mon;
    *lpDay = ptmNow->tm_mday;// + 1
}

static
int  CheckExpirationPeriod_Internal(const HKEY hKeyParent,
                                    const std::wstring& subKey,
                                    const std::wstring& value,
                                    uint32_t expirationPeriod)
{
    int ret = e_validation_failure;

    WinRegAPI keyUser { hKeyParent, subKey };

    if (keyUser.Create(hKeyParent, subKey)) {

        size_t dataSize = 0;
        unsigned char *pBinaryData = nullptr;
        if (false == keyUser.GetBinaryValue(value, &pBinaryData, &dataSize)) {
            int year, month, day;
            size_t memSize = ((k_random_cnt + 3) << 1) + 2;

            pBinaryData = (unsigned char*)malloc(memSize);
            if (!pBinaryData)
                return e_validation_failure;

            GetCurrentDateTime(true, false, &year, &month, &day);

            srand((unsigned int)time(NULL));

            int idx = 0;
            for (int i = 0; i < k_random_cnt; ++i)
                pBinaryData[idx++] = (unsigned char)(rand() % 255);

            pBinaryData[idx++] = (unsigned char)(rand() % 255);
            pBinaryData[idx++] = (unsigned char)(year);
            pBinaryData[idx++] = (unsigned char)(rand() % 255);
            pBinaryData[idx++] = (unsigned char)(month);
            pBinaryData[idx++] = (unsigned char)(rand() % 255);
            pBinaryData[idx++] = (unsigned char)(day);

            for (int i = 0; i < k_random_cnt; ++i)
                pBinaryData[idx++] = (unsigned char)(rand() % 255);

            uint16_t crc16 = GetCRC16Ibm(pBinaryData, idx);
            pBinaryData[idx++] = (uint8_t)(crc16 >> 8);
            pBinaryData[idx++] = (uint8_t)(crc16 & 0x00FF);
            keyUser.SetBinaryValue(value, pBinaryData, idx);

            ret = e_expiration_ok;
        }
        else {
            uint16_t crc16Calc = GetCRC16Ibm(pBinaryData, dataSize - 2);
            uint16_t crc16Real = (pBinaryData[dataSize - 2] << 8) | pBinaryData[dataSize - 1];
            if (crc16Calc == crc16Real) {
                int oldYear = (int)pBinaryData[k_random_cnt + 1];
                int oldMonth = (int)pBinaryData[k_random_cnt + 3];
                int oldDay = (int)pBinaryData[k_random_cnt + 5];

                int year, month, day;
                GetCurrentDateTime(true, false, &year, &month, &day);

                struct std::tm tmOld = { 0,0,0, oldDay, oldMonth, oldYear };
                struct std::tm tmNew = { 0,0,0, day, month, year };
                std::time_t timeOld = std::mktime(&tmOld);
                std::time_t timeNew = std::mktime(&tmNew);
                if (timeOld != (std::time_t)(-1) && timeNew != (std::time_t)(-1))
                {
                    double difference = std::difftime(timeNew, timeOld) / (60 * 60 * 24);
                    if ((difference >= 0) && (difference <= expirationPeriod))
                        ret = e_expiration_ok;
                    else
                        ret = e_expiration_failure;
                }
            }
            else
                ret = e_validation_failure;
        }
        if (pBinaryData) { free(pBinaryData); pBinaryData = nullptr; }
    }

    return ret;
}

bool PreValidate()
{
    bool ret = false;

    //size_t memSize = ((k_random_cnt + 3) << 1) + 2;
    size_t dataSize = 0;
    unsigned char *pBinaryData = nullptr;

    WinRegAPI keyUser{ g_user_key, g_user_subkey };
    if ((true == keyUser.Open(g_user_key, g_user_subkey)) &&
        (true == keyUser.GetBinaryValue(g_user_value, &pBinaryData, &dataSize))) {
        if (pBinaryData) { free(pBinaryData); pBinaryData = nullptr; }
        WinRegAPI keyUser2{ g_machine_key, g_machine_subkey };
        if ((true == keyUser2.Open(g_machine_key, g_machine_subkey)) &&
            (true == keyUser2.GetBinaryValue(g_machine_value, &pBinaryData, &dataSize))) {
            if (pBinaryData) { free(pBinaryData); pBinaryData = nullptr; }
                ret = true;
        }
    }
    else
    {
        WinRegAPI keyUser2{ g_machine_key, g_machine_subkey };
        if ((false == keyUser2.Open(g_machine_key, g_machine_subkey)) &&
            (false == keyUser2.GetBinaryValue(g_machine_value, &pBinaryData, &dataSize)))
            ret = true;
        if (pBinaryData) { free(pBinaryData); pBinaryData = nullptr; }
    }

    return ret;
}

int  CheckExpirationPeriod(uint32_t expirationPeriod)
{
    int ret = e_validation_failure;

    if (PreValidate())
    {
        if ((e_expiration_ok == (ret = CheckExpirationPeriod_Internal(g_user_key, g_user_subkey, g_user_value, expirationPeriod))) &&
            (e_expiration_ok == (ret = CheckExpirationPeriod_Internal(g_machine_key, g_machine_subkey, g_machine_value, expirationPeriod))))
            ret = e_expiration_ok;
    }

    if (e_expiration_ok != ret)
        DeleteValidationKey();

    return ret;
}

void  DeleteValidationKey()
{
    {
        WinRegAPI key{ g_user_key, g_user_subkey };
        if (key.Open(g_user_key, g_user_subkey))
            key.DeleteValue(g_user_value);
    }
    {
        WinRegAPI key{ g_machine_key, g_machine_subkey };
        if (key.Open(g_machine_key, g_machine_subkey))
            key.DeleteValue(g_machine_value);
    }
}

int  ValidationTest(const TCHAR *validationFileName,
                    const TCHAR *validationString)
{
    int ret = e_validation_failure;
    uint32_t expiredPeriod = 0;

    FILE *p = _tfopen(validationFileName, L"rb");
    if (p) {
        bool pass = true;
        fseek(p, k_random_cnt, SEEK_SET);
        for (int i = 0; i < _tcslen(validationString); ++i) {
            unsigned char data;
            fread((void*)&data, 1, 1, p);
            if (data != validationString[i]) {
                pass = false;
                break;
            }
        }
        if (pass) {
            for (int i = 0; i < sizeof(uint32_t); ++i) {
                unsigned char tmp;
                fread((void*)&tmp, 1, 1, p);
                expiredPeriod |= tmp << (8 * (3 - i));
            }
            // round 2
            if (expiredPeriod > 0)
                ret = CheckExpirationPeriod(expiredPeriod);
        }

        fclose(p);
    }
    else
        ret = e_validation_failure;

    return ret;
}
