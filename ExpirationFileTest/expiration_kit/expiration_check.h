#ifndef EXPIRATION_CHECK_H
#define EXPIRATION_CHECK_H

#include <stdint.h>
#include <tchar.h>

#include "expiration_config.h"

typedef enum E_EXPIRATION_RETURN_TYPE_TAG { 
    e_validation_failure = -9, e_expiration_failure = -8, e_expiration_ok = 0,
} EExpirationReturnType;

void GetCurrentDateTime(bool useLocalTime, bool shiftYear, int *lpYear, int *lpMonth, int *lpDay);
int  CheckExpirationPeriod(uint32_t expirationPeriod);
void DeleteValidationKey();
int  ValidationTest(const TCHAR *validationFileName, const TCHAR *validationString);

#endif // EXPIRATION_CHECK_H
