#include "crc16.h"

#ifndef UINT16_MAX
#define UINT16_MAX (UINT16_C(0xffff))
#endif

#ifndef UINT32_MAX
#define UINT32_MAX (UINT32_C(0xffffffff))
#endif
/*==============================================================================
 *  Definitions of local const variables
 *============================================================================*/
const uint16_t k_crc16_tab[256] = {
    UINT16_C(0x0000), UINT16_C(0x8005), UINT16_C(0x800f), UINT16_C(0x000a),
    UINT16_C(0x801b), UINT16_C(0x001e), UINT16_C(0x0014), UINT16_C(0x8011),
    UINT16_C(0x8033), UINT16_C(0x0036), UINT16_C(0x003c), UINT16_C(0x8039),
    UINT16_C(0x0028), UINT16_C(0x802d), UINT16_C(0x8027), UINT16_C(0x0022),
    UINT16_C(0x8063), UINT16_C(0x0066), UINT16_C(0x006c), UINT16_C(0x8069),
    UINT16_C(0x0078), UINT16_C(0x807d), UINT16_C(0x8077), UINT16_C(0x0072),
    UINT16_C(0x0050), UINT16_C(0x8055), UINT16_C(0x805f), UINT16_C(0x005a),
    UINT16_C(0x804b), UINT16_C(0x004e), UINT16_C(0x0044), UINT16_C(0x8041),
    UINT16_C(0x80c3), UINT16_C(0x00c6), UINT16_C(0x00cc), UINT16_C(0x80c9),
    UINT16_C(0x00d8), UINT16_C(0x80dd), UINT16_C(0x80d7), UINT16_C(0x00d2),
    UINT16_C(0x00f0), UINT16_C(0x80f5), UINT16_C(0x80ff), UINT16_C(0x00fa),
    UINT16_C(0x80eb), UINT16_C(0x00ee), UINT16_C(0x00e4), UINT16_C(0x80e1),
    UINT16_C(0x00a0), UINT16_C(0x80a5), UINT16_C(0x80af), UINT16_C(0x00aa),
    UINT16_C(0x80bb), UINT16_C(0x00be), UINT16_C(0x00b4), UINT16_C(0x80b1),
    UINT16_C(0x8093), UINT16_C(0x0096), UINT16_C(0x009c), UINT16_C(0x8099),
    UINT16_C(0x0088), UINT16_C(0x808d), UINT16_C(0x8087), UINT16_C(0x0082),

    UINT16_C(0x8183), UINT16_C(0x0186), UINT16_C(0x018c), UINT16_C(0x8189),
    UINT16_C(0x0198), UINT16_C(0x819d), UINT16_C(0x8197), UINT16_C(0x0192),
    UINT16_C(0x01b0), UINT16_C(0x81b5), UINT16_C(0x81bf), UINT16_C(0x01ba),
    UINT16_C(0x81ab), UINT16_C(0x01ae), UINT16_C(0x01a4), UINT16_C(0x81a1),
    UINT16_C(0x01e0), UINT16_C(0x81e5), UINT16_C(0x81ef), UINT16_C(0x01ea),
    UINT16_C(0x81fb), UINT16_C(0x01fe), UINT16_C(0x01f4), UINT16_C(0x81f1),
    UINT16_C(0x81d3), UINT16_C(0x01d6), UINT16_C(0x01dc), UINT16_C(0x81d9),
    UINT16_C(0x01c8), UINT16_C(0x81cd), UINT16_C(0x81c7), UINT16_C(0x01c2),
    UINT16_C(0x0140), UINT16_C(0x8145), UINT16_C(0x814f), UINT16_C(0x014a),
    UINT16_C(0x815b), UINT16_C(0x015e), UINT16_C(0x0154), UINT16_C(0x8151),
    UINT16_C(0x8173), UINT16_C(0x0176), UINT16_C(0x017c), UINT16_C(0x8179),
    UINT16_C(0x0168), UINT16_C(0x816d), UINT16_C(0x8167), UINT16_C(0x0162),
    UINT16_C(0x8123), UINT16_C(0x0126), UINT16_C(0x012c), UINT16_C(0x8129),
    UINT16_C(0x0138), UINT16_C(0x813d), UINT16_C(0x8137), UINT16_C(0x0132),
    UINT16_C(0x0110), UINT16_C(0x8115), UINT16_C(0x811f), UINT16_C(0x011a),
    UINT16_C(0x810b), UINT16_C(0x010e), UINT16_C(0x0104), UINT16_C(0x8101),

    UINT16_C(0x8303), UINT16_C(0x0306), UINT16_C(0x030c), UINT16_C(0x8309),
    UINT16_C(0x0318), UINT16_C(0x831d), UINT16_C(0x8317), UINT16_C(0x0312),
    UINT16_C(0x0330), UINT16_C(0x8335), UINT16_C(0x833f), UINT16_C(0x033a),
    UINT16_C(0x832b), UINT16_C(0x032e), UINT16_C(0x0324), UINT16_C(0x8321),
    UINT16_C(0x0360), UINT16_C(0x8365), UINT16_C(0x836f), UINT16_C(0x036a),
    UINT16_C(0x837b), UINT16_C(0x037e), UINT16_C(0x0374), UINT16_C(0x8371),
    UINT16_C(0x8353), UINT16_C(0x0356), UINT16_C(0x035c), UINT16_C(0x8359),
    UINT16_C(0x0348), UINT16_C(0x834d), UINT16_C(0x8347), UINT16_C(0x0342),
    UINT16_C(0x03c0), UINT16_C(0x83c5), UINT16_C(0x83cf), UINT16_C(0x03ca),
    UINT16_C(0x83db), UINT16_C(0x03de), UINT16_C(0x03d4), UINT16_C(0x83d1),
    UINT16_C(0x83f3), UINT16_C(0x03f6), UINT16_C(0x03fc), UINT16_C(0x83f9),
    UINT16_C(0x03e8), UINT16_C(0x83ed), UINT16_C(0x83e7), UINT16_C(0x03e2),
    UINT16_C(0x83a3), UINT16_C(0x03a6), UINT16_C(0x03ac), UINT16_C(0x83a9),
    UINT16_C(0x03b8), UINT16_C(0x83bd), UINT16_C(0x83b7), UINT16_C(0x03b2),
    UINT16_C(0x0390), UINT16_C(0x8395), UINT16_C(0x839f), UINT16_C(0x039a),
    UINT16_C(0x838b), UINT16_C(0x038e), UINT16_C(0x0384), UINT16_C(0x8381),

    UINT16_C(0x0280), UINT16_C(0x8285), UINT16_C(0x828f), UINT16_C(0x028a),
    UINT16_C(0x829b), UINT16_C(0x029e), UINT16_C(0x0294), UINT16_C(0x8291),
    UINT16_C(0x82b3), UINT16_C(0x02b6), UINT16_C(0x02bc), UINT16_C(0x82b9),
    UINT16_C(0x02a8), UINT16_C(0x82ad), UINT16_C(0x82a7), UINT16_C(0x02a2),
    UINT16_C(0x82e3), UINT16_C(0x02e6), UINT16_C(0x02ec), UINT16_C(0x82e9),
    UINT16_C(0x02f8), UINT16_C(0x82fd), UINT16_C(0x82f7), UINT16_C(0x02f2),
    UINT16_C(0x02d0), UINT16_C(0x82d5), UINT16_C(0x82df), UINT16_C(0x02da),
    UINT16_C(0x82cb), UINT16_C(0x02ce), UINT16_C(0x02c4), UINT16_C(0x82c1),
    UINT16_C(0x8243), UINT16_C(0x0246), UINT16_C(0x024c), UINT16_C(0x8249),
    UINT16_C(0x0258), UINT16_C(0x825d), UINT16_C(0x8257), UINT16_C(0x0252),
    UINT16_C(0x0270), UINT16_C(0x8275), UINT16_C(0x827f), UINT16_C(0x027a),
    UINT16_C(0x826b), UINT16_C(0x026e), UINT16_C(0x0264), UINT16_C(0x8261),
    UINT16_C(0x0220), UINT16_C(0x8225), UINT16_C(0x822f), UINT16_C(0x022a),
    UINT16_C(0x823b), UINT16_C(0x023e), UINT16_C(0x0234), UINT16_C(0x8231),
    UINT16_C(0x8213), UINT16_C(0x0216), UINT16_C(0x021c), UINT16_C(0x8219),
    UINT16_C(0x0208), UINT16_C(0x820d), UINT16_C(0x8207), UINT16_C(0x0202)
};

const uint16_t k_crc_poly = UINT16_C(0x8005);


/*==============================================================================
 *  prototype of functions
 *============================================================================*/
 uint16_t GetCRC16Ibm(const uint8_t *, size_t);

 /*==============================================================================
 *  Definitions of static Functions
 *============================================================================*/
 static
 uint32_t GetUInt32(const uint8_t* pData) {
     if (!pData) return UINT32_C(0x00000000);

     return static_cast<uint32_t>(((pData[0] << 24) |
                                   (pData[1] << 16) |
                                   (pData[2] << 8) |
                                   (pData[3])));
 }
/*==============================================================================
 *  Definitions of global Functions
 *============================================================================*/
uint16_t GetCRC16Ibm(const uint8_t *pBuffer, size_t length) {
    register uint16_t crc = UINT16_C(0xFFFF);
    const uint8_t *pBuf = pBuffer;
    size_t nLen = length;
    uint8_t a1;
    uint8_t a2;
    uint8_t a3;
    uint8_t a4;
    uint8_t a5;
    uint8_t a6;
    uint8_t a7;
    uint8_t a8;

    for (; nLen >= 32; nLen -= 32) {
        register uint32_t data;

        data = GetUInt32(pBuf);
        pBuf += 4;

        crc = (crc << 8) ^ k_crc16_tab[((crc >> 8) ^ (data >> 24)) & 0xFF];
        crc = (crc << 8) ^ k_crc16_tab[((crc >> 8) ^ (data >> 16)) & 0xFF];
        crc = (crc << 8) ^ k_crc16_tab[((crc >> 8) ^ (data >>  8)) & 0xFF];
        crc = (crc << 8) ^ k_crc16_tab[((crc >> 8) ^ (data >>  0)) & 0xFF];
    }

    switch (nLen >> 3) {
    case 3:
        crc = (crc << 8) ^ k_crc16_tab[((crc >> 8) ^ *pBuf++) & 0xFF];
    case 2:
        crc = (crc << 8) ^ k_crc16_tab[((crc >> 8) ^ *pBuf++) & 0xFF];
    case 1:
        crc = (crc << 8) ^ k_crc16_tab[((crc >> 8) ^ *pBuf++) & 0xFF];
        nLen %= 8;
    case 0:
        break;
    }

    a1 = (*pBuf >> 7) & 0x01;
    a2 = (*pBuf >> 6) & 0x01;
    a3 = (*pBuf >> 5) & 0x01;
    a4 = (*pBuf >> 4) & 0x01;
    a5 = (*pBuf >> 3) & 0x01;
    a6 = (*pBuf >> 2) & 0x01;
    a7 = (*pBuf >> 1) & 0x01;
    a8 = *pBuf & 0x01;

    while (nLen--) {
        register unsigned int msb;

        switch (nLen)
        {
        case 8:
            msb = a1 ^ (crc >> 15);
            break;
        case 7:
            msb = a2 ^ (crc >> 15);
            break;
        case 6:
            msb = a3 ^ (crc >> 15);
            break;
        case 5:
            msb = a4 ^ (crc >> 15);
            break;
        case 4:
            msb = a5 ^ (crc >> 15);
            break;
        case 3:
            msb = a6 ^ (crc >> 15);
            break;
        case 2:
            msb = a7 ^ (crc >> 15);
            break;
        case 1:
            msb = a8 ^ (crc >> 15);
            break;
        default:
            msb = 0;
        }

        crc <<= 1;
        if (msb & 1)
            crc ^= k_crc_poly;
    }

    return crc & UINT16_C(0xFFFF);
}

